﻿namespace KipEight.Core
{
    public interface IGraphicsRenderer
    {
        void ClearScreen();
        bool DrawSprite(byte[] sprite, byte x, byte y);
    }
}