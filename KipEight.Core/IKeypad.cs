﻿namespace KipEight.Core
{
    public interface IKeypad
    {
        bool IsKeyPressed(int id);
    }
}