﻿using System;
using System.Security.Cryptography;

namespace KipEight.Core
{
    public class EmuEngine
    {
        public byte[] Registers { get; private set; }
        public byte[] Memory { get; set; }
        
        public ushort I { get; private set; }

        public byte Delay { get; private set; }
        public byte Sound { get; private set; }

        public byte StackPointer { get; private set; }
        public ushort ProgramCounter { get; private set; }

        public ushort[] Stack { get; private set; }

        public uint InstructionsPerSecond { get; set; } = 1200;

        private readonly IGraphicsRenderer _renderer;
        private readonly ISoundRenderer _soundRenderer;
        private readonly IKeypad _keypad;
        private readonly Random _random;
        private readonly byte[] _randomBuffer;
        private readonly ushort _entrypoint;

        public EmuEngine(IGraphicsRenderer renderer, ISoundRenderer soundRenderer, IKeypad keypad, ushort entrypoint = 0x200)
        {
            _renderer = renderer;
            _soundRenderer = soundRenderer;
            _keypad = keypad;
            _entrypoint = entrypoint;
            _random = new Random();
            _randomBuffer = new byte[1];
            Reset();
        }

        private void Reset()
        {
            Registers = new byte[16];
            Memory = new byte[0x1000];
            Stack = new ushort[16];

            ProgramCounter = _entrypoint;

            var fonts = new byte[]
            {
                0xF0, 0x90, 0x90, 0x90, 0xF0,
                0x20, 0x60, 0x20, 0x20, 0x70,
                0xF0, 0x10, 0xF0, 0x80, 0xF0,
                0x90, 0x90, 0xF0, 0x10, 0x10,
                0xF0, 0x80, 0xF0, 0x10, 0xF0,
                0xF0, 0x80, 0xF0, 0x90, 0xF0,
                0xF0, 0x10, 0x20, 0x40, 0x40,
                0xF0, 0x90, 0xF0, 0x90, 0xF0,
                0xF0, 0x90, 0xF0, 0x90, 0x90,
                0xE0, 0x90, 0xE0, 0x90, 0xE0,
                0xF0, 0x80, 0x80, 0x80, 0xF0,
                0xE0, 0x90, 0x90, 0x90, 0xE0,
                0xF0, 0x80, 0xF0, 0x80, 0xF0,
                0xF0, 0x80, 0xF0, 0x80, 0x80
            };

            Buffer.BlockCopy(fonts, 0, Memory, 0, fonts.Length);
        }

        private byte GetRandomByte()
        {
            _random.NextBytes(_randomBuffer);
            return _randomBuffer[0];
        }

        public void EraseMemory()
        {
            for (var i = _entrypoint; i < Memory.Length - _entrypoint; i++)
            {
                Memory[i] = 0;
            }
        }

        public void Load(byte[] rom) => Load(rom, _entrypoint);
        public void Load(byte[] rom, ushort address) => Buffer.BlockCopy(rom, 0, Memory, address, rom.Length);

        public void Step(uint timeBudget)
        {
            var cycles = InstructionsPerSecond * (timeBudget / 1000d);

            for (uint i = 0; i < cycles; i++)
            {
                var msb = Memory[ProgramCounter++];
                var lsb = Memory[ProgramCounter++];

                var opcode = (ushort) ((msb << 8) | lsb);
                var dbg = opcode.ToString("X4");
                Execute(opcode);
            }
        }

        private void Execute(ushort opcode)
        {
            if (opcode == 0x00E0) // CLS
            {
                _renderer.ClearScreen();
                return;
            }

            if (opcode == 0x00EE) // RET
            {
                ProgramCounter = Stack[StackPointer];
                StackPointer--;
                return;
            }

            if (opcode < 0x1000) // SYS addr - ignore
                return;

            if (opcode < 0x2000) // JP addr
            {
                ProgramCounter = (ushort) (opcode & 0x0FFF);
                return;
            }
            
            if (opcode < 0x3000) // CALL addr
            {
                StackPointer++;
                StackPointer = (byte) (StackPointer % 16);
                Stack[StackPointer] = (ushort) (opcode & 0x0FFF);
                return;
            }

            if (opcode < 0x4000) // SE Vx, byte
            {
                var x = (opcode & 0x0F00) >> 8;
                var value = opcode & 0x00FF;
                var regValue = Registers[x];
                if (value == regValue)
                    ProgramCounter += 2;
                return;
            }

            if (opcode < 0x5000) // SNE Vx, byte
            {
                var x = (opcode & 0x0F00) >> 8;
                var value = opcode & 0x00FF;
                var regValue = Registers[x];
                if (value != regValue)
                    ProgramCounter += 2;
                return;
            }

            if (opcode < 0x6000) // SE Vx, Vy
            {
                var subOp = opcode & 0x000F;
                if (subOp != 0)
                    return;

                var x = (opcode & 0x0F00) >> 8;
                var y = (opcode & 0x00F0) >> 4;

                var xValue = Registers[x];
                var yValue = Registers[y];

                if (xValue == yValue)
                    ProgramCounter += 2;

                return;
            }

            if (opcode < 0x7000) // LD Vx, byte
            {
                var x = (opcode & 0x0F00) >> 8;
                var value = opcode & 0x00FF;

                Registers[x] = (byte) value;
                return;
            }

            if (opcode < 0x8000) // ADD Vx, byte
            {
                var x = (opcode & 0x0F00) >> 8;
                var value = opcode & 0x00FF;
                Registers[x] = (byte) (Registers[x] + value);
                return;
            }

            if (opcode < 0x9000)
            {
                var x = (opcode & 0x0F00) >> 8;
                var y = (opcode & 0x00F0) >> 4;
                var subOp = opcode & 0x000F;

                byte vx, vy;

                switch (subOp)
                {
                    case 0: // LD Vx, Vy
                        Registers[x] = Registers[y];
                        return;

                    case 1: // OR Vx, Vy
                        Registers[x] |= Registers[y];
                        return;

                    case 2: // AND Vx, Vy
                        Registers[x] &= Registers[y];
                        return;

                    case 3: // XOR Vx, Vy
                        Registers[x] ^= Registers[y];
                        return;

                    case 4: // ADD Vx, Vy
                        var result = Registers[x] + Registers[y];
                        Registers[0xF] = (byte) (result > 0xFF ? 1 : 0);
                        Registers[x] = (byte) result;
                        return;

                    case 5: // SUB Vx, Vy
                        vx = Registers[x];
                        vy = Registers[y];
                        // TODO borrow
                        Registers[x] = (byte) (vx - vy);
                        return;

                    case 6: // SHR Vx {, Vy}
                        vx = Registers[x];
                        Registers[0xF] = (byte) ((vx & 0x01) == 0x01 ? 1 : 0);
                        Registers[x] = (byte) (vx >> 1);
                        return;

                    case 7: // SUBN Vx, Vy
                        vx = Registers[x];
                        vy = Registers[y];
                        Registers[0xF] = (byte) (vy > vx ? 1 : 0);
                        return;

                    case 0xE: // SHL Vx {, Vy}
                        vx = Registers[x];
                        Registers[0xF] = (byte) ((vx & 0x80) == 0x80 ? 1 : 0);
                        Registers[x] = (byte) (vx << 1);
                        return;

                    default: // NOP
                            return;
                }
            }

            if (opcode < 0xA000) // SNE Vx, Vy
            {
                var subOp = opcode & 0x000F;
                if (subOp != 0)
                    return;

                var x = (opcode & 0x0F00) >> 8;
                var y = (opcode & 0x00F0) >> 4;
                var vx = Registers[x];
                var vy = Registers[y];

                if (vx != vy)
                    ProgramCounter += 2;
                return;
            }

            if (opcode < 0xB000) // LD I, addr
            {
                var value = opcode & 0x0FFF;
                I = (ushort) value;
                return;
            }

            if (opcode < 0xC000) // JP V0, addr
            {
                var value = opcode & 0x0FFF;
                var destination = Registers[0] + value;
                ProgramCounter = (ushort) destination;
                return;
            }

            if (opcode < 0xD000) // RND Vx, byte
            {
                var value = (byte) (opcode & 0x00FF);
                var rnd = GetRandomByte();
                rnd %= value;
                var x = (opcode & 0x0F00) >> 8;
                Registers[x] = rnd;
                return;
            }

            if (opcode < 0xE000) // DRW Vx, Vy, nibble
            {
                var ix = (byte) ((opcode & 0x0F00) >> 8);
                var iy = (byte) ((opcode & 0x00F0) >> 4);
                var x = Registers[ix];
                var y = Registers[iy];
                var n = (opcode & 0x000F);

                var buffer = new byte[n];
                Buffer.BlockCopy(Memory, I, buffer, 0, n);
                var result = _renderer.DrawSprite(buffer, x, y);
                Registers[0xF] = (byte) (result ? 1 : 0);
                return;
            }

            var xParam = (opcode & 0x0F00) >> 8;
            var maskedOp = opcode & 0xF0FF;
            switch (maskedOp)
            {
                case 0xE09E: // SKP Vx
                    if (_keypad.IsKeyPressed(xParam))
                        ProgramCounter += 2;
                    return;

                case 0xE0A1: // SKNP Vx
                    if (!_keypad.IsKeyPressed(xParam))
                        ProgramCounter += 2;
                    return;

                case 0xF007: // LD Vx, DT
                    Registers[xParam] = Delay;
                    return;

                case 0xF00A: // LD Vx, K
                    for (var i = 0; i < 16; i++)
                    {
                        var isPressed = _keypad.IsKeyPressed(i);
                        if (!isPressed)
                            continue;

                        Registers[xParam] = (byte) i;
                        return;
                    }

                    ProgramCounter -= 2;
                    return;

                case 0xF015: // LD DT, Vx
                    Delay = Registers[xParam];
                    return;

                case 0xF018: // LD ST, Vx
                    Sound = Registers[xParam];
                    return;

                case 0xF01E: // ADD I, Vx
                    var x = Registers[xParam];
                    I += x;
                    return;

                case 0xF029: // LD F, Vx
                    var value = Registers[xParam];
                    I = (ushort) (value * 5);
                    return;

                case 0xF033: // LD B, Vx
                    var reg = Registers[xParam];
                    var (h, d, u) = GetBcd(reg);
                    Memory[I] = h;
                    Memory[I + 1] = d;
                    Memory[I + 2] = u;
                    return;

                case 0xF055: // LD [I], Vx
                    Buffer.BlockCopy(Registers, 0, Memory, I, xParam);
                    return;

                case 0xF065: // LD Vx, [I]
                    Buffer.BlockCopy(Memory, I, Registers, 0, xParam);
                    return;

                default:
                    return;
            }
        }

        // Ugly hack ahead
        private static (byte, byte, byte) GetBcd(byte reg)
        {
            var str = reg.ToString("D3");
            var h = byte.Parse(str.Substring(0, 1));
            var d = byte.Parse(str.Substring(1, 1));
            var u = byte.Parse(str.Substring(2, 1));

            return (h, d, u);
        }
    }
}
