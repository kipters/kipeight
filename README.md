# KipEight

Portable CHIP-8 compiler core and console frontend written in C# and born out of
boredom.

Instantiate an `EmuEngine` object passing an implementation of each of the 
required interfaces, load a rom and start calling `Step()`.
The parameter is the time budget in milliseconds.

## What's working

### Engine

1. Instruction decoding and most of the opcodes (not sure if they _all_ work as 
2. expected)
2. Registers
3. Stack

### Console frontend

1. Text mode graphics renderer
2. Audio renderer
3. Basic initialization and execution

## What's missing

### Engine

1. Timers
2. May need some work to keep better track of cycles

### Console frontend

1. Keypad handler
2. Execution loop
3. Move to .NET Core since it's 2018 already

---

Released under [WTFPL](http://www.wtfpl.net/)