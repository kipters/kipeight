﻿using System.IO;
using KipEight.Core;
using static System.Console;

namespace KipEight.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var graphics = new ConsoleRenderer(64, 32);
            var sound = new BeepRenderer();
            var keypad = new PcKeypad();
            var engine = new EmuEngine(graphics, sound, keypad);

            var rom = File.ReadAllBytes("trip.ch8");
            engine.Load(rom);
            engine.Step(uint.MaxValue);

            ReadLine();
        }
    }
}
