﻿using System;
using System.Diagnostics;
using KipEight.Core;

namespace KipEight.Console
{
    internal class PcKeypad : IKeypad
    {
        private readonly ConsoleKey[] _keys;

        public PcKeypad()
        {
            _keys = new[]
            {
                ConsoleKey.D0,
                ConsoleKey.D1,
                ConsoleKey.D2,
                ConsoleKey.D3,
                ConsoleKey.D4,
                ConsoleKey.D5,
                ConsoleKey.D6,
                ConsoleKey.D7,
                ConsoleKey.D8,
                ConsoleKey.D9,
                ConsoleKey.Q,
                ConsoleKey.W,
                ConsoleKey.E,
                ConsoleKey.R,
                ConsoleKey.T,
                ConsoleKey.Y,
            };
        }

        public bool IsKeyPressed(int id)
        {
            //Debug.WriteLine($"Reading 0x{id:x1}");

            if (!System.Console.KeyAvailable)
                return false;

            var target = _keys[id];
            while (System.Console.KeyAvailable)
            {
                var key = System.Console.ReadKey(true);
                if (key.Key == target)
                    return true;
            }

            return false;
        }
    }
}