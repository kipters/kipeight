﻿using KipEight.Core;

namespace KipEight.Console
{
    internal class BeepRenderer : ISoundRenderer
    {
        public void Beep()
        {
            System.Console.Beep(400, 100);
        }
    }
}