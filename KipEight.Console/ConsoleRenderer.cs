﻿using KipEight.Core;
using static System.Console;

namespace KipEight.Console
{
    public class ConsoleRenderer : IGraphicsRenderer
    {
        private readonly int _width;
        private readonly int _height;
        private bool[,] _framebuffer;

        private const char PixelOn = (char) 9608;
        private const char PixelOff = ' ';

        public ConsoleRenderer(int width, int height)
        {
            _width = width;
            _height = height;
            CursorVisible = false;
            SetWindowSize(_width * 2, _height);
            ClearScreen();
        }

        public void ClearScreen()
        {
            _framebuffer = new bool[_width, _height];
            Clear();
        }

        public bool DrawSprite(byte[] sprite, byte x, byte y)
        {
            var eraseFlag = false;
            CursorLeft = x * 2;
            CursorTop = y;

            for (var line = 0; line < sprite.Length; line++)
            {
                var row = sprite[line];
                CursorTop++;
                for (var column = 0; column < 8; column++)
                {
                    var rx = x + column;
                    var ry = y + line;
                    var isOn = (row & 0x80) == 0x80;
                    var current = _framebuffer[rx, ry];
                    var result = current ^ isOn;

                    if (result)
                        eraseFlag = true;

                    _framebuffer[rx, ry] = result;

                    var c = result ? PixelOn : PixelOff;
                    Write(c);
                    Write(c);
                    row = (byte)(row << 1);
                }

                CursorLeft = x * 2;
            }
            
            return eraseFlag;
        }
    }
}
