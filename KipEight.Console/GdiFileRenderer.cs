﻿using System.Drawing;
using System.Drawing.Imaging;
using KipEight.Core;

namespace KipEight.Console
{
    internal class GdiFileRenderer : IGraphicsRenderer
    {
        private readonly Bitmap _bitmap;
        private bool[,] _framebuffer;

        public GdiFileRenderer()
        {
            _bitmap = new Bitmap(64, 32, PixelFormat.Format32bppArgb);
            ClearScreen();
        }

        public void ClearScreen()
        {
            _framebuffer = new bool[64, 32];
            using (var g = Graphics.FromImage(_bitmap))
            {
                g.Clear(Color.Black);
            }
        }

        private int _last = 0;

        public bool DrawSprite(byte[] sprite, byte x, byte y)
        {
            var eraseFlag = false;
            for (var line = 0; line < sprite.Length; line++)
            {
                var row = sprite[line];
                for (var column = 0; column < 8; column++)
                {
                    var rx = x + column;
                    var ry = y + line;
                    var isOn = (row & 0x80) == 0x80;
                    var current = _framebuffer[rx, ry];
                    var result = current ^ isOn;

                    if (result)
                        eraseFlag = true;

                    _framebuffer[rx, ry] = result;
                    var color = result ? Color.White : Color.Black;
                    _bitmap.SetPixel(x + column, y + line, color);
                    row = (byte) (row << 1);
                }
            }

            _bitmap.Save($"{_last++}.png", ImageFormat.Png);
            return eraseFlag;
        }
    }
}